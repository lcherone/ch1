<?php
/**
 *  @name      CH1 - A Basic Empty MVC PHP Framework
 *  @author    Lawrence Cherone - http://cherone.co.uk
 *  @version   rev0.1a
 */
if (!defined("RUN")){exit(header ("Location: ./"));}


define('BASE_FOLDER', rtrim('/Git_Projects/CH1','/'));
define('SITE_ROOT',   pathinfo($_SERVER['SCRIPT_FILENAME'], PATHINFO_DIRNAME));
define('SITE_URL',    'http://'.$_SERVER['HTTP_HOST'].BASE_FOLDER);
?>