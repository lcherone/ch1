<?php
/**
 *  @name      CH1 - A Basic Empty MVC PHP Framework
 *  @author    Lawrence Cherone - http://cherone.co.uk
 *  @version   rev0.1a
 */
if (!defined("RUN")){exit(header ("Location: ./"));}

/*
 * Simple Config loader, load all files from: /core/config/*
 */
foreach (glob("application/config/*.php") as $filename) {
	if(basename($filename) == basename(__FILE__)) continue;
	require_once($filename);
}
?>