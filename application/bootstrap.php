<?php
/**
 *  @name      CH1 - A Basic Empty MVC PHP Framework
 *  @author    Lawrence Cherone - http://cherone.co.uk
 *  @version   rev0.1a
 */
if (!defined("RUN")) exit(header ("Location: ./"));

/**
 * Load Core Framework
 */
require_once('./application/config/loader.php');
require_once('./application/framework/core.php');
require_once('./application/framework/functions.php');
require_once('./application/framework/router.php');
require_once('./application/framework/controller.php');
require_once('./application/framework/model.php');
require_once('./application/framework/template.php');

/**
 * Initialize Framework
 */
$core = new core;
$core->function = new functions($core);
$core->function->gentime();
$core->model = new model($core);
$core->router = new router($core);
$core->template = new template($core);
$core->router->run_controller();
?>
