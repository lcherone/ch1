<?php
/**  __  __            _  _         ____   _
 *  |  \/  |  ___   __| |(_)  __ _ / ___| | |_  _ __  ___   __ _  _ __ ___
 *  | |\/| | / _ \ / _` || | / _` |\___ \ | __|| '__|/ _ \ / _` || '_ ` _ \
 *  | |  | ||  __/| (_| || || (_| | ___) || |_ | |  |  __/| (_| || | | | | |
 *  |_|  |_| \___| \__,_||_| \__,_||____/  \__||_|   \___| \__,_||_| |_| |_|
 *  @name      MediaStream
 *  @author    Lawrence Cherone - http://cherone.co.uk
 *  @version   rev0.1a
 *
 * NOT EVEN CLOSE TO FINNISHED
 */
if (!defined("RUN")){exit(header ("Location: ./"));}

Class api_controller extends controller{
	private $data;
	private $model;

	function __construct($core){
		/* run parent construct */
		parent::__construct($core);
		/* assign data array */
		$this->data  = array();
		/* load page model */
		$this->model['page'] = $this->core->model->load('page_model', $this->core);
	}

	function index(){
		//Tell template class there is no view output
		$this->core->template->output = false;
		header('Content-Type: text/plain');
		echo "Nothing here yet";
	}
}
?>