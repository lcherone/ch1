<?php
/**
 *  @name      CH1 - A Basic Empty MVC PHP Framework
 *  @author    Lawrence Cherone - http://cherone.co.uk
 *  @version   rev0.1a
 */
if (!defined("RUN")){exit(header ("Location: ./"));}


Class not_found_controller extends controller{
	private $data;
	private $model;

	function __construct($core){
		parent::__construct($core);
		/* load page model */
		$this->model['page'] = $this->core->model->load('page_model', $this->core);
	}

	function index(){
		$this->data  = array();
		header("HTTP/1.0 404 Not Found");

		$this->core->template->title = "404 Page not found";
		$this->core->template->loadView('not_found/page_not_found', 'content', $this->data);

		$this->core->template->loadView('partials/sidebar', 'sidebar', $this->data);
	}
}
?>