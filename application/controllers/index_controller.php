<?php
/**
 *  @name      CH1 - A Basic Empty MVC PHP Framework
 *  @author    Lawrence Cherone - http://cherone.co.uk
 *  @version   rev0.1a
 */
if (!defined("RUN")){exit(header ("Location: ./"));}

Class index_controller extends controller{
	private $data;
	private $model;

	function __construct($core){
		/* run parent construct */
		parent::__construct($core);
		/* assign data array */
		$this->data  = array();
		/* load page model */
		$this->model['page']  = $this->core->model->load('page_model', $this->core);
	}

	function index(){
		/*query page model for page content */
		$this->data['page_result'] = $this->model['page']->get_page('index');

		/* pass data to view and assign to template variables */
		$this->core->template->loadView('index/index', 'content', $this->data);
		$this->core->template->loadView('partials/sidebar', 'sidebar', $this->data);
	}
}
?>