<?php
/**
 *  @name      CH1 - A Basic Empty MVC PHP Framework
 *  @author    Lawrence Cherone - http://cherone.co.uk
 *  @version   rev0.1a
 */
if (!defined("RUN")){exit(header ("Location: ./"));}

Class load_controller extends controller{

	function __construct($core){
		parent::__construct($core);

		$path = explode('/',$this->core->router->request);
		if(empty($path[1])){
			exit(header('Location: '.SITE_URL));
		}

		$this->type      = !empty($path[1])?    basename(trim($path[1])):null; //type [css,image,js]
		$this->filename  = !empty($path[2])?'/'.basename(trim($path[2])):null; //filename
		$this->filename .= !empty($path[3])?'/'.basename(trim($path[3])):null; //.=filename
		$this->filename .= !empty($path[4])?'/'.basename(trim($path[4])):null; //.=filename
		$this->filename .= !empty($path[5])?'/'.basename(trim($path[5])):null; //.=filename
		$this->filename .= !empty($path[6])?'/'.basename(trim($path[6])):null; //.=filename
		$this->filename .= !empty($path[7])?'/'.basename(trim($path[7])):null; //.=filename
		if(empty($this->filename)){
			$this->filename  = $this->type;
			$this->type = null;
		}

		if($this->type == 'media_thumb'){
			$this->filepath = SITE_ROOT.'/shared_media/thumbnails/'.$this->filename;
		}else{
			$this->filepath = SITE_ROOT.'/application/template/'.(!empty($this->type) ? $this->type : null).$this->filename;
		}

	}

	function index(){
		$forbidden['filenames']  = array('index.php','info.xml','.htaccess');
		$forbidden['directorys'] = array('views');

		if(!in_array($this->type, array($forbidden['directorys'])) && !in_array(basename($this->filepath), $forbidden['filenames'])){
			if(file_exists($this->filepath) && !is_dir($this->filepath)){
				$this->core->template->output = false;
				$this->content_type = $this->core->function->get_mime_by_extension($this->filename);
				$this->cache_expire = (string)(8 * 86400);
				// Set resource last modified time
				$this->modified = filemtime($this->filepath);

				if(isset($_SESSION['theme_changed'])){
					header("Cache-Control: no-cache, must-revalidate");
					header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
					header('Location: '.$_SERVER['REQUEST_URI']);
					unset($_SESSION['theme_changed']);
					exit;
				}

				// Check client cache if found send 304
				if(isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) && strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) == $this->modified){
					exit(header($_SERVER['SERVER_PROTOCOL'] . ' 304 Not Modified'));
				}else{
					// Send responce headers
					header('Pragma: public');
					header("Cache-Control: max-age={$this->cache_expire}, pre-check={$this->cache_expire}, private, proxy-revalidate");
					header('Expires: ' . gmdate('D, d M Y H:i:s \G\M\T', time() + $this->cache_expire));
					header('Last-Modified: ' . gmdate('D, d M Y H:i:s \G\M\T', $this->modified));
					header("Content-Type: {$this->content_type}");
					//Start loading file into $out variable
					set_time_limit(0);
					$out = null;
					$h = gzopen($this->filepath,'rb');
					if($h){
						while ($line = gzgets($h, 4096)){
							$out .= $line;
						}
					}else{
						header("HTTP/1.0 410 Gone");
						die('Error loading '.$this->filepath);
					}
					gzclose($h);
					//CSS Replace {SITE_URL} with real url
					$out = $this->css_replace_urls($out);
					header('Content-Length: '.strlen($out));
					echo $out;
				}
			}else{
				/* 404 - Page not found */
				header("HTTP/1.0 404 Not Found");
				$this->core->template->title = "404 Page not found";
				$this->core->template->loadView('not_found/page_not_found', 'content');
				$this->core->template->loadView('index/sidebar', 'sidebar');
			}
		}else{
			exit(header('Location: '.SITE_URL));
		}
	}

	private function css_replace_urls($src){
		$search  = array('{SITE_URL}');
		$replace = array(SITE_URL);
		return str_replace($search,$replace,$src);
	}

}
?>