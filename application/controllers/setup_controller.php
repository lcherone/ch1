<?php
/**
 *  @name      CH1 - A Basic Empty MVC PHP Framework
 *  @author    Lawrence Cherone - http://cherone.co.uk
 *  @version   rev0.1a
 */
if (!defined("RUN")){exit(header ("Location: ./"));}

Class setup_controller extends controller{
	private $data;

	function __construct($core){
		/* run parent construct */
		parent::__construct($core);
		/* assign data array */
		$this->data  = array();
		/* load page model */
		$this->model['page'] = $this->core->model->load('page_model', $this->core);
		$this->model['auth'] = $this->core->model->load('auth_model', $this->core);
	}

	function index(){
		/* Check Setup */
		if(empty($this->core->settings->installed)){
			/* Install Triggered */
			if($_SERVER['REQUEST_METHOD'] == 'POST'){
				//Create admin account and login
				if($this->model['auth']->create_admin(5)===false){
					$this->data['error'] = $this->model['auth']->auth->error;
				}

				$this->core->settings->setup_database();
				exit(header('Location: '.SITE_URL));
			}
			//assign secure form keys
			$_SESSION['csrf']      = sha1(uniqid().(microtime(true)+1));
			$_SESSION['userParam'] = sha1(uniqid().(microtime(true)+2));
			$_SESSION['passParam'] = sha1(uniqid().(microtime(true)+3));

			/* pass data to view and assign to template var */
			$this->core->template->loadView('setup/setup_index', 'content', $this->data);

		}//Is Installed
		else{
			$this->core->template->loadView('setup/setup_complete', 'content', $this->data);
		}

	}
}
?>