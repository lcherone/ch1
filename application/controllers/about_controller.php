<?php
/**
 *  @name      CH1 - A Basic Empty MVC PHP Framework
 *  @author    Lawrence Cherone - http://cherone.co.uk
 *  @version   rev0.1a
 */
if (!defined("RUN")){exit(header ("Location: ./"));}

Class about_controller extends controller{
	private $data;
	private $model;

	function __construct($core){
		parent::__construct($core);
		/* load page model */
		$this->model['page'] = $this->core->model->load('page_model', $this->core);
	}

	function index(){
		$this->data  = array();

		$this->core->template->loadView('about/index', 'content', $this->data);
		$this->core->template->loadView('partials/sidebar', 'sidebar', $this->data);
	}
}
?>