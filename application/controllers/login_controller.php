<?php
/**
 *  @name      CH1 - A Basic Empty MVC PHP Framework
 *  @author    Lawrence Cherone - http://cherone.co.uk
 *  @version   rev0.1a
 */
if (!defined("RUN")){exit(header ("Location: ./"));}

Class login_controller extends controller{
	private $data;
	private $model;

	function __construct($core){
		parent::__construct($core);
		$this->data  = array();
		/* load page model */
		$this->model['page'] = $this->core->model->load('page_model', $this->core);
		$this->model['auth'] = $this->core->model->load('auth_model', $this->core);
	}

	function index(){
		/* do login */
		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			if($this->model['auth']->process_login()===false){
				$this->data['error'] = $this->model['auth']->auth->error;
			}
		}

		/* not logged in */
		if(empty($_SESSION['logged_in'])){

			/* assign secure form keys */
			$_SESSION['csrf']      = sha1(uniqid().(microtime(true)+1));
			$_SESSION['userParam'] = sha1(uniqid().(microtime(true)+2));
			$_SESSION['passParam'] = sha1(uniqid().(microtime(true)+3));

			$this->core->template->loadView('partials/login', 'content', $this->data);
			$this->core->template->loadView('partials/sidebar', 'sidebar', $this->data);
		}else{
			exit(header('Location: ./admin'));
		}
	}

}
?>