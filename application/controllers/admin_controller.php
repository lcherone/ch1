<?php
/**
 *  @name      CH1 - A Basic Empty MVC PHP Framework
 *  @author    Lawrence Cherone - http://cherone.co.uk
 *  @version   rev0.1a
 */
if (!defined("RUN")){exit(header ("Location: ./"));}

Class admin_controller extends controller{
	private $data;
	private $model;

	function __construct($core){
		parent::__construct($core);
		$this->data  = array();
		/* start auth model */
		$this->model['auth'] = $this->core->model->load('auth_model', $this->core);
		/* load page model */
		$this->model['page'] = $this->core->model->load('page_model', $this->core);
		/* start admin model */
		$this->model['admin'] = $this->core->model->load('admin_model', $this->core);
	}

	function index(){
		/* do login */
		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			if($this->model['auth']->process_login() === false){
				$this->data['error'] = $this->model['auth']->error;
			}
		}
		/* logged in */
		if($this->model['auth']->check_login()){
			$this->core->template->loadView('admin/index', 'content', $this->data);
			$this->core->template->loadView('partials/sidebar', 'sidebar', $this->data);
		}else{
			exit(header('Location: '.SITE_URL.'/login'));
		}
	}

	function logout(){
		$this->model['auth']->auth->logout();
	}

}
?>