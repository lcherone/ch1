<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<title><?php echo $this->core->settings->sitename; ?><?php echo ($this->core->router->controller != 'index') ? ' - '.ucwords(str_replace('_',' ',$this->core->router->controller)) : null;?></title>
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<link href="<?php echo SITE_URL;?>/load/css/style.css" rel="stylesheet" media="screen">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>
</head>

<body>

<div class="wrapper">
	<header class="header">
		<h1><?php echo $this->core->settings->sitename; ?><em>rev0.1</em></h1>
		<div id="nav">
			<?php echo $nav_bar; ?>
		</div>
	</header>
	<div class="middle">
		<div class="container">
			<main class="content">
				<div class="content_wrap">
					<?php echo $content; ?>
				</div>
			</main>
		</div>
		<aside class="sidebar">
			<?php echo $sidebar; ?>
		</aside>
	</div>
	<footer class="footer">
		<p><?php echo $this->core->settings->sitename; ?> By <a href="http://cherone.co.uk/<?php echo $this->core->settings->sitename; ?>">Lawrence Cherone</a></p>
		<p><span class="nowrap"><b style="color:#464040;">Memory Usage:</b> <?php echo $this->core->function->memory_usage(); ?></span>
		   <span class="nowrap"><b style="color:#464040;">Processing time:</b> <?php echo $this->core->function->gentime(); ?> seconds</span>
		</p>
	</footer>
</div>

</body>
</html>