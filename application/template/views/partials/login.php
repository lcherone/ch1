<style>
#login_form{margin-top:25px;}
#login_form table {
	width:350px;
}
#login_form table span {
	font-weight:bold;
	color:#464040;
	padding-left:10px;
}
#login_form #error {
	float:right;
	margin-top:5px;
	margin-right:10px;
	color:#FF6962 !important;
	font-size:.87em !important;
}
#login_form > table > tbody > tr > td > input {
	border:0;
	padding:5px;
	font-weight:bold;
	background:#A5CDB3;
	margin-bottom:2px;
	color: #f0f0f0;
	text-shadow: 0px 0px 3px rgba(0,0,0, .5);
}
.login_form_button {
	float: right;
	margin-top:-2px;
	padding-top: 4px !important;
	padding-right: 7px !important;
	padding-bottom: 4px !important;
	padding-left: 7px !important;
	border:0;
	-webkit-border-radius:0 0 5px 5px;
	-moz-border-radius:0 0 5px 5px;
	border-radius:0 0 5px 5px;
	font-weight:bold;
	background:#DF815B !important;
    box-shadow: inset 0px 0px 8px rgba(255,255,255, .5), 0px 1px 0px rgba(255,255,255, .8) ;
	color: #f0f0f0;
	text-shadow: 0px 0px 3px rgba(0,0,0, .5);
}
</style>

<h1>Admin Login</h1>


<?php
//echo '<pre>'.print_r($_SESSION,true).'</pre>';
//echo '<pre>'.print_r($error['global'],true).'</pre>';

echo $this->core->function->form_escape('
<form method="POST" action="" id="login_form">
	<input type="hidden" name="csrf" value="'.$_SESSION['csrf'].'">

	<table style="border-collapse: collapse">
	  <tr>
	    <td width="34%" valign="bottom"><span>Username :&nbsp;</span></td>
	    <td width="66%"><input type="text" name="'.$_SESSION['userParam'].'" size="30" required></td>
	  </tr>
	  <tr>
	    <td width="34%"><span>Password :&nbsp;</span></td>
	    <td width="66%"><input type="password" name="'.$_SESSION['passParam'].'" size="30" required></td>
	  </tr>
	  <tr>
	    <td width="100%" colspan="2"><input class="login_form_button" type="submit" value="Login"> '.(isset($error['global'])?'<span id="error">'.$error['global'].'</span>':null).'</td>
	  </tr>
	</table>
</form>
');
?>