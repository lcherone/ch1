<h1>Re-Setup MediaStream</h1>
<p>If you want to re-setup media stream simply delete both <b>application/databases/users.db</b> and <b>application/databases/media.db</b> database files and then refresh this page.</p>
<style>
	.content{width:100%;}
	.content_wrap{width:80%;max-width:1000px;margin-left:auto;margin-right:auto;}
	.sidebar{display:none;}
</style>