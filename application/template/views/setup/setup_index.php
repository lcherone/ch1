<style>
.clear{clear:both;}
#setup_form{margin-top:25px;}
#setup_form table {
	width:100%;
	float:left;
	margin-left:-7%;
}
#setup_form table span {
	font-weight:bold;
	font-size:.8em;
	color:#464040;
	padding-left:10px;
	float:right;
	padding-right:10px;
}
#setup_form #error {
	float:right;
	margin-top:5px;
	margin-right:10px;
	color:#FF6962 !important;
	font-size:.87em !important;
}
#setup_form > table > tbody > tr > td > input {
	border:0;
	padding:5px;
	font-weight:bold;
	background:#A5CDB3;
	margin-bottom:2px;
	color: #f0f0f0;
	text-shadow: 0px 0px 3px rgba(0,0,0, .5);
}
.setup_form_button {
	float: right;
	margin-top:10px;
	margin-right:6%;
	padding-top: 4px !important;
	padding-right: 7px !important;
	padding-bottom: 4px !important;
	padding-left: 7px !important;
	border:0;
	-webkit-border-radius:5px;
	-moz-border-radius:5px;
	border-radius:5px;
	font-weight:bold;
	background:#DF815B !important;
    box-shadow: inset 0px 0px 8px rgba(255,255,255, .5), 0px 1px 0px rgba(255,255,255, .8) ;
	color: #f0f0f0;
	text-shadow: 0px 0px 3px rgba(0,0,0, .5);
}

.notice p {text-align:center;}
tr{ background:none !important; }
</style>
<h1>Setup CH1</h1>
<div class="notice">
	<p>CH1 is a example MVC framework, created by Lawrence cherone.</p>
	<p style="color:#FF6962;font-weight:bold;">Use only the latest release from <a href="http://cherone.co.uk">http://cherone.co.uk</a></p>
</div>

<?php echo (isset($error['global'])?'<span id="error">'.$error['global'].'</span>':null) ?>

<form method="POST" action="" id="setup_form">
	<input type="hidden" name="csrf" value="<?php echo $_SESSION['csrf'] ?>">

	<h1>Admin Username &amp; Password</h1>

	<table style="border-collapse: collapse">
	  <tr>
	    <td width="35%" valign="bottom" nowrap="nowrap"><span>Username :&nbsp;</span></td>
	    <td width="65%"><input type="text" name="<?php echo $_SESSION['userParam'] ?>" style="width:90%" required></td>
	  </tr>
	  <tr>
	    <td nowrap="nowrap"><span>Password :&nbsp;</span></td>
	    <td><input type="password" name="<?php echo $_SESSION['passParam'] ?>" style="width:90%" required></td>
	  </tr>
	  <tr>
	    <td width="100%" colspan="2"><input class="setup_form_button" type="submit" value="Install Script"></td>
	  </tr>
	</table>
</form>
<style>
	.content{width:100%;}
	.content_wrap{width:80%;max-width:1000px;margin-left:auto;margin-right:auto;}
	.sidebar{display:none;}
</style>