<h1>About <?php echo $this->core->settings->sitename; ?></h1>

<div style="text-align:center;">
	<p><?php echo $this->core->settings->sitename; ?> is an empty MVC example framework.</p>
	<p>Powered PHP with SQLite3(PDO), with lots of common models available.</p>
	<br>
	<p>Created by Lawrence Cherone as a somewhat intresting example script that I can put
	   on my <a href="http://cherone.co.uk"><b>site</b></a>, so let me know your comments ;-S
	</p>
	<br>
	<p>You can also <a href="https://github.com/lcherone/<?php echo $this->core->settings->sitename; ?>">fork a copy from github</a></p>
</div>