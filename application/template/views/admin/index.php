<h1>Admin Area</h1>
<p>Here is a basic admin interface to add and remove pages and users.</p>
<br>

<h1>Manage Pages</h1>
<p><a href="<?php echo SITE_URL;?>/admin/user/add">Add page</a></p>

<table>
	<thead>
		<tr>
			<th width="74%">Username</th>
			<th width="10%"></th>
			<th width="1%"></th>
			<th width="1%"></th>
			<th width="14%"></th>
		</tr>
	</thead>
	<tbody>
	<?php if(!empty($media_result)): ?>
	<?php foreach($media_result as $file): ?>
		<tr class="hover_tr">
			<td><a href="<?php echo SITE_URL.'/watch/'.$file['filehash'] ?>" data-movie="<?php echo $file['filehash'] ?>"><?php echo ucfirst($file['filename']) ?></a>
				<span class="ajax_details" style="float:right;"></span>
			</td>
			<td class="media_date"><?php echo date("d-m-y", $file['filedate']) ?></td>
			<td class="media_watch"><a href="<?php echo SITE_URL.'/watch/'.$file['filehash'] ?>"><span class="television"></span></a></td>
			<td class="media_download"><a href="<?php echo SITE_URL.'/download/'.$file['filehash'] ?>"><span class="disk"></span></a></td>
			<td class="media_size"><?php echo $this->core->function->format_bytes($file['filesize']) ?></td>
		</tr>
	<?php endforeach; ?>
	<?php else: ?>
		<tr>
			<td colspan="5">No users found</td>
		</tr>
	<?php endif; ?>
	</tbody>
</table>

<h1>Manage Users</h1>
<p><a href="<?php echo SITE_URL;?>/admin/user/add">Add user</a></p>

<table>
	<thead>
		<tr>
			<th width="74%">Username</th>
			<th width="10%"></th>
			<th width="1%"></th>
			<th width="1%"></th>
			<th width="14%"></th>
		</tr>
	</thead>
	<tbody>
	<?php if(!empty($media_result)): ?>
	<?php foreach($media_result as $file): ?>
		<tr class="hover_tr">
			<td><a href="<?php echo SITE_URL.'/watch/'.$file['filehash'] ?>" data-movie="<?php echo $file['filehash'] ?>"><?php echo ucfirst($file['filename']) ?></a>
				<span class="ajax_details" style="float:right;"></span>
			</td>
			<td class="media_date"><?php echo date("d-m-y", $file['filedate']) ?></td>
			<td class="media_watch"><a href="<?php echo SITE_URL.'/watch/'.$file['filehash'] ?>"><span class="television"></span></a></td>
			<td class="media_download"><a href="<?php echo SITE_URL.'/download/'.$file['filehash'] ?>"><span class="disk"></span></a></td>
			<td class="media_size"><?php echo $this->core->function->format_bytes($file['filesize']) ?></td>
		</tr>
	<?php endforeach; ?>
	<?php else: ?>
		<tr>
			<td colspan="5">No users found</td>
		</tr>
	<?php endif; ?>
	</tbody>
</table>


<h1>Settings</h1>
