<?php
/**
 *  @name      CH1 - A Basic Empty MVC PHP Framework
 *  @author    Lawrence Cherone - http://cherone.co.uk
 *  @version   rev0.1a
 */
if (!defined("RUN")){exit(header ("Location: ./"));}

/* This Class is started in the Router Class (constructor), it contains common
functions used throughout the app
*/
class functions {
	protected $core;

	function __construct($core){
		$this->core = $core;
	}

	/**
	 * Get mime type by extension:
	 * mime array @ /application/config/mime.types.php
	 */
	function get_mime_by_extension($filename){
		$extension = strtolower(substr(strrchr($filename, '.'), 1));
		$mimes = SITE_ROOT.'/application/config/mime.types.php';
		try{
			if(!file_exists($mimes)){
				throw new Exception('Core Exception: Mime array file not found! ('.$mimes.')');
			}else{
				@require($mimes);
				if (!is_array($mime_types)){
					throw new Exception('Core Exception: Mime array ('.$mimes.') is loaded but $mime_types array was not found.!');
				}else{
					if (array_key_exists($extension, $mime_types)){
						if (is_array($mime_types[$extension])){
							return current($mime_types[$extension]);
						}else{
							return $mime_types[$extension];
						}
					}else{
						return false;
					}
				}
			}
		}catch (Exception $e){
			die($e->getMessage());
		}
		return false;
	}

	function format_bytes($bytes){
		if ($bytes < 1024) return $bytes.' B';
		elseif ($bytes < 1048576) return round($bytes / 1024, 2).' KB';
		elseif ($bytes < 1073741824) return round($bytes / 1048576, 2).' MB';
		elseif ($bytes < 1099511627776) return round($bytes / 1073741824, 2).' GB';
		else return round($bytes / 1099511627776, 2).' TB';
	}

	function gentime() {
		static $a;
		if($a == 0) $a = microtime(true);
		else return sprintf("%.3f", (float) microtime(true) - $a);
	}

	function memory_usage(){
		return $this->format_bytes(memory_get_peak_usage()).'/'.ini_get("memory_limit").'B';
	}

	function pagination($total, $p, $lpm1, $prev, $next){
		$return = null;
		$adjacents  = 3;
		$s_term = isset($_GET['s']) ? '&s='.htmlspecialchars(ucwords(strip_tags($_GET['s']))) : null;

		if($total > 1){
			$return .= "
		<style>
		.pagination{
			 padding-top: 4px !important;
			 padding-right: 7px !important;
			 padding-bottom: 4px !important;
			 padding-left: 7px !important;
			 -webkit-border-radius: 5px;
			 -moz-border-radius: 5px;
			 border-radius: 5px;
			 font-weight:bold;
			-webkit-border-radius: 3px;
			-moz-border-radius: 3px;
			border-radius: 3px;
		 	border-style: solid;
		 	border-color:#DF815B;
			border-width: 1px;
			line-height:32px;
		}
		.pagination:hover{
			border-color:#A5CDB3;
			border-width: 1px;
		}
		.pagination.active {
			border:none;
		    background: #96B7C6;
		    box-shadow: inset 0px 0px 8px rgba(255,255,255, .5), 0px 1px 0px rgba(255,255,255, .8);
		    color: #f0f0f0;
		    text-shadow: 0px 0px 3px rgba(0,0,0, .5);
		    cursor:default !important;
		}
		</style>
		<div style=\"margin-top:15px;margin-bottom:15px;text-align:center;\">";
			//previous button
			if($p > 1)
			$return .= "<a href=\"?pg={$prev}{$s_term}\" class=\"pagination\"><<&nbsp;Previous</a> ";
			else
			$return .= "<span class=\"pagination active\"><<&nbsp;Previous</span> ";

			if ($total < 7 + ($adjacents * 2)) {
				for ($counter = 1; $counter <= $total; $counter++) {
					if ($counter == $p)
					$return .= "<span class=\"pagination active\">$counter</span>";
					else
					$return .= " <a href=\"?pg=$counter{$s_term}\" class=\"pagination\">$counter</a> ";}
			}elseif($total > 5 + ($adjacents * 2)){
				if($p < 1 + ($adjacents * 2)){
					for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++){
						if ($counter == $p)
						$return .= " <span class=\"pagination active\">$counter</span> ";
						else
						$return .= " <a href=\"?pg=$counter{$s_term}\" class=\"pagination\">$counter</a> ";
					}
					$return .= " ... ";
					$return .= " <a href=\"?pg=$lpm1{$s_term}\" class=\"pagination\">$lpm1</a> ";
					$return .= " <a href=\"?pg=$total{$s_term}\" class=\"pagination\">$total</a> ";
				}
				//middle
				elseif($total - ($adjacents * 2) > $p && $p > ($adjacents * 2)){
					$return .= " <a href=\"?pg=1{$s_term}\" class=\"pagination\">1</a> ";
					$return .= " <a href=\"?pg=2{$s_term}\" class=\"pagination\">2</a> ";
					$return .= " ... ";
					for ($counter = $p - $adjacents; $counter <= $p + $adjacents; $counter++){
						if ($counter == $p)
						$return .= " <span class=\"pagination active\">$counter</span> ";
						else
						$return .= " <a href=\"?pg=$counter{$s_term}\" class=\"pagination\">$counter</a> ";
					}
					$return .= " ... ";
					$return .= " <a href=\"?pg=$lpm1{$s_term}\" class=\"pagination\">$lpm1</a> ";
					$return .= " <a href=\"?pg=$total{$s_term}\" class=\"pagination\">$total</a> ";
				}else{
					$return .= " <a href=\"?pg=1{$s_term}\" class=\"pagination\">1</a> ";
					$return .= " <a href=\"?pg=2{$s_term}\" class=\"pagination\">2</a> ";
					$return .= " ... ";
					for ($counter = $total - (2 + ($adjacents * 2)); $counter <= $total; $counter++){
						if ($counter == $p)
						$return .= " <span class=\"pagination active\">$counter</span> ";
						else
						$return .= " <a href=\"?pg=$counter{$s_term}\" class=\"pagination\">$counter</a> ";
					}
				}
			}
			if ($p < $counter - 1)
			$return .= " <a href=\"?pg=$next{$s_term}\" class=\"pagination\">Next&nbsp;>></a>";
			else
			$return .= " <span class=\"pagination active\">Next&nbsp;>></span>";
			$return .= "</div>\n";
		}
		return $return;
	}

	/**
	 * Create a javascript escaped string.
	 */
	function form_escape($str){
		$str = preg_replace('/^\s+|\n|\r|\s+$/m', '', $str);
		$enc = '';
		for ($i=0; $i < strlen($str); $i++){
			$hex = dechex(ord($str[$i]));
			$enc .= ($hex=='') ? $enc.urlencode($str[$i]) : '%'.((strlen($hex)==1)?('0'.strtoupper($hex)):(strtoupper($hex)));
		}
		$enc = str_replace(array('.','+','_','-'),array('%2E','%20','%5F','%2D'),$enc);
		$sec = substr(sha1(microtime(true)),0,5);
		return '<script type="text/javascript">var x'.$sec.'x="'.$enc.'";document.write(unescape(x'.$sec.'x));</script>
		<noscript>
			<style>
				#noscript_notice {
					text-align: center;
					font-weight: bold;
					color:#FF6962;
					padding-top: 20px;
				}
			</style>
			<div id="noscript_notice">
				<p>Please enable JavaScript!</p>
			</div>
		</noscript>'.PHP_EOL;
	}

	function thumbnail($path, $save_path="", $maxw=120, $maxh=150){
		$type = pathinfo($path, PATHINFO_EXTENSION);
		if($type=='jpg'){$img = imagecreatefromjpeg($path);}
		if($type=='png'){$img = imagecreatefrompng($path);}
		if($type=='gif'){$img = imagecreatefromgif($path);}
		// width & height of original image
		$width = imagesx($img);
		$height = imagesy($img);
		// determine which side is the longest to use in calculating length of the shorter side
		if ($height > $width){
			$ratio = $maxh / $height;
			$newheight = $maxh;
			$newwidth = $width * $ratio;
		}else{
			$ratio = $maxw / $width;
			$newwidth = $maxw;
			$newheight = $height * $ratio;
		}
		// create new image resource
		$newimg = imagecreatetruecolor($newwidth,$newheight);
		// keep transparency
		imagealphablending($newimg, false);
		imagesavealpha($newimg,true);
		imagefilledrectangle($newimg, 0, 0, $newwidth, $newheight, imagecolorallocatealpha($newimg, 255, 255, 255, 127));
		// assign color palette to new image
		for ($i = 0; $i < imagecolorstotal($img); $i++) {
			$colors = imagecolorsforindex($img, $i);
			imagecolorallocate($newimg, $colors['red'], $colors['green'], $colors['blue']);
		}
		// copy original image into new image at new size.
		imagecopyresampled($newimg, $img, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
		// save
		$save_path = $save_path.'th_'.$maxw.'_'.basename($path);
		if($type=='jpg'){ imagejpeg($newimg, $save_path); }
		if($type=='png'){ imagepng($newimg, $save_path);  }
		if($type=='gif'){ imagegif($newimg, $save_path);  }
		if(file_exists($save_path)){
			return $save_path;
		}else{
			return false;
		}
	}

	/**
	 * CURL GET function
	 */
	function curl_get($url, $referer = null){
		if($referer == null) $referer = $url;

		$header = array();
		$header[] = "Accept: text/xml,application/xml,application/json,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5";
		$header[] = "Cache-Control: max-age=0";
		$header[] = "Connection: keep-alive";
		$header[] = "Keep-Alive: 300";
		$header[] = "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7";
		$header[] = "Accept-Language: en-us,en;q=0.5";

		//Check curl is installed or revert to file_get_contents()
		$return = (function_exists('curl_init')) ? 'true' : 'false';

		if($return == 'false'){
			$opts = array('http'=>array(
			'method'=>"GET",
			'header'=>implode('\r\n',$header)."\r\n".
			"Referer: $referer\r\n",
			'user_agent'=> "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:24.0) Gecko/20100101 Firefox/24.0"));

			$context = stream_context_create($opts);
			return file_get_contents($url, false, $context);
		}

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:24.0) Gecko/20100101 Firefox/24.0");
		curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
		curl_setopt($curl, CURLOPT_HEADER, 0);
		curl_setopt($curl, CURLOPT_REFERER, $referer);
		curl_setopt($curl, CURLOPT_ENCODING, 'gzip,deflate');
		curl_setopt($curl, CURLOPT_AUTOREFERER, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_TIMEOUT, 30);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

		$html = curl_exec($curl);
		curl_close($curl);
		return $html;
	}

	function slugify($text){
		// replace non letter or digits by -
		$text = preg_replace('~[^\\pL\d]+~u', '-', $text);
		// trim
		$text = trim($text, '-');
		// transliterate
		$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
		// lowercase
		$text = strtolower($text);
		// remove unwanted characters
		$text = preg_replace('~[^-\w]+~', '', $text);
		if (empty($text)){
			return 'n-a';
		}
		return $text;
	}

	function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
		$sort_col = array();
		foreach($arr as $key=>$row){
			$sort_col[$key] = $row[$col];
		}
		return array_multisort($sort_col, $dir, $arr);
	}

}
?>