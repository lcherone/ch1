<?php
/**
 *  @name      CH1 - A Basic Empty MVC PHP Framework
 *  @author    Lawrence Cherone - http://cherone.co.uk
 *  @version   rev0.1a
 */
if (!defined("RUN")){exit(header ("Location: ./"));}

/**
 * Template class
 */
Class template{
	protected $core;
	protected $vars = array();
	function __construct($core) {
		$this->core = $core;
	}
	public function __set($index, $value){$this->vars[$index] = $value;}
	public function __get($index){return $this->vars[$index];}

	function loadView($view, $section=null, $data=null){
		$this->path = SITE_ROOT.'/application/template/views/'.$view.'.php';

		/* check view exists */
		try {
			if (file_exists($this->path) === false){
				throw new Exception('<span style="font-weight:bold;color:red;">Content view ('.htmlentities($view).') not found:</span> '.htmlentities($this->path).'<br>');
				return false;
			}
		}catch (Exception $e){
			die($e->getMessage());
		}

		/* extract $data passed to this method or extract $this*/
		if($data != null){
			extract($data);
		}else{
			extract((array)$this);
		}

		ob_start();
		require($this->path);
		$return = ob_get_contents();
		ob_end_clean();

		/* assign content to $this->core->template->{$section} */
		if($section !== null){
			$this->vars[$section] = $return;
		}

		// perhaps replace something
		$search  = array('{SITE_URL}');
		$replace = array(SITE_URL);
		$return  = str_replace($search,$replace,$return);

		/* return */
		return $return;
	}


	/**
	 * Render final output using variables assigned within the controllers
	 */
	public function display() {
		$content = null;
		if(!empty($this->vars)){
			extract($this->vars);
		}

		if($this->core->template->output === true):
		$this->path = SITE_ROOT.'/application/template/index.php';

		/* check view exists */
		try {
			if (file_exists($this->path) === false){
				throw new Exception('<span style="font-weight:bold;color:red;">Template not found:</span> '.htmlentities($this->path).'<br>');
				return false;
			}
		}catch (Exception $e){
			die($e->getMessage());
		}

		ob_start();
		require($this->path);
		$return = ob_get_contents();
		ob_end_clean();

		// perhaps replace something
		$search  = array('{SITE_URL}');
		$replace = array(SITE_URL);
		$return  = str_replace($search,$replace,$return);

		echo $return;
		else:
		exit($content);
		endif;
	}
}
?>