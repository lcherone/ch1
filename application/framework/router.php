<?php
/**
 *  @name      CH1 - A Basic Empty MVC PHP Framework
 *  @author    Lawrence Cherone - http://cherone.co.uk
 *  @version   rev0.1a
 */
if (!defined("RUN")){exit(header ("Location: ./"));}

/**
 * Routing class, this class will handle input paramiters for controller routing.
 */
Class router{
	private $core;
	public $controller;
	public $action;
	public $subaction;
	public $request;

	function __construct($core) {
		$this->core = $core;
		$this->process_inputs();
	}

	/**
	 * Check for magic quotes & invalid keys
	 */
	function process_inputs(){
		$inputs = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST, &$_FILES);
		array_walk_recursive($inputs, array(__CLASS__,'process'));
	}

	private function process(&$value, $key){
			// magic quotes fix
			if (get_magic_quotes_gpc()) {
				$key   = stripslashes($key);
				$value = stripslashes($value);
			}
			// null byte fix
			$key   = str_replace(chr(0), '', $key);
			$value = str_replace(chr(0), '', $value);
	}

	/**
	 * Set controller values from route.
	 */
	private function set_controller() {
		$this->request = !isset($_GET['route']) ? '' : $_GET['route'];
		/* split the parts of the route */
		$parts = explode('/', $this->request);

		/* trying to access __construct && __destruct are forbidden */
		if(in_array('__construct',$parts) || in_array('__destruct',$parts))
			exit(header('Location: '.SITE_URL));

		/* set controller */
		$this->controller = str_replace('-','_',$parts[0]);
		/* Site.com/action */
		$this->action = isset($parts[1]) ? $parts[1] : null;
		/* Site.com/action/subaction */
		$this->subaction = isset($parts[2]) ? $parts[2] : null;
		/* Site.com/action/subaction/subaction_id */
		$this->subaction_id = isset($parts[3]) ? $parts[3] : null;
		/* fix controller */
		if(empty($this->controller)) $this->controller = 'index';
		/* fix action */
		if(empty($this->action)) $this->action = 'index';
	}

	/**
	 * Run controller.
	 */
	public function run_controller(){
		/* set routing */
		$this->set_controller();

		/* create controllers class instance & inject core */
		$controller = SITE_ROOT.'/application/controllers/'.$this->controller.'_controller.php';
		if(file_exists($controller)){
			require_once($controller);
			$class = $this->controller.'_controller';
			if(class_exists($class)){
				$this->core->controller = new $class($this->core);
			}
		}else{
			require_once(SITE_ROOT.'/application/controllers/not_found_controller.php');
			$this->core->controller = new not_found_controller($this->core);
		}

		/* check the root class is callable */
		if (is_callable(array($this->core->controller, $this->action)) === false){
			/* index() method because not found method */
			$action = 'index';
		}else{
			/* action() method is callable */
			$action = $this->action;
		}
		/* run the controllers action method */
		return $this->core->controller->{$action}();
	}
}
?>