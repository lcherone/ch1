<?php
/**
 *  @name      CH1 - A Basic Empty MVC PHP Framework
 *  @author    Lawrence Cherone - http://cherone.co.uk
 *  @version   rev0.1a
 */
if (!defined("RUN")){exit(header ("Location: ./"));}

/**
 * Model class, this class is the base model for the routes models.
 */
Class model{

	function __construct($core) {
		$this->core = $core;
	}

	/* model loader */
	function load(){
		// retrieve passed arguments
		$_args = func_get_args();
		// delete the first argument which is the class name
		$_class = array_shift($_args);
		try{
			// check for class file
			if(file_exists(SITE_ROOT.'/application/models/'.$_class.'.php')){
				require_once(SITE_ROOT.'/application/models/'.$_class.'.php');
				// remove the lib/ path part
				if(substr($_class,0, 3)=='lib') $_class = substr($_class, 4);
				// initialise class using reflection
				$_ref = new ReflectionClass($_class);
				return $_ref->newInstanceArgs($_args);
			}else{
				throw new Exception('Model class not found: '.SITE_ROOT.'/application/models/'.$_class.'.php');
			}
		}catch(Exception $e){
			return '<span style="color:red">'.$e->getMessage().'</span>';
		}
	}

}
?>