<?php
/**
 *  @name      CH1 - A Basic Empty MVC PHP Framework
 *  @author    Lawrence Cherone - http://cherone.co.uk
 *  @version   rev0.1a
 */
if (!defined("RUN")){exit(header ("Location: ./"));}

/**
 * Core class, this class will hold values and objects,
 * the wrapper for all other classes and values.
 */
Class core{
	private $vars = array();

	public function __set($index, $value){
		$this->vars[$index] = $value;
	}

	public function __get($index){
		return $this->vars[$index];
	}

	private function __clone(){}
}
?>