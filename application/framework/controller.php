<?php
/**
 *  @name      CH1 - A Basic Empty MVC PHP Framework
 *  @author    Lawrence Cherone - http://cherone.co.uk
 *  @version   rev0.1a
 */
if (!defined("RUN")){exit(header ("Location: ./"));}

/**
 * Controller class, this class is the base controller for sub controllers.
 */
Abstract Class controller{
	protected $core;

	function __construct($core) {
		$this->core = $core;
		/* Set some defaults */
		$this->core->template->title   = null;
		$this->core->template->content = null;
		$this->core->template->sidebar = null;
		$this->core->template->nav_bar = $this->set_nav();
		$this->core->template->output  = true;
		/* get settings */
		$this->core->settings = $this->core->model->load('settings_model', $this->core);
		/* check script is installed */
		if($this->core->router->controller != 'setup' && empty($this->core->settings->installed)){
			exit(header('Location: '.SITE_URL.'/setup'));
		}
	}
	/* Abstract, so controllers must have index methods */
	abstract function index();

	/* Assign Navigation */
	function set_nav(){
		return $this->core->template->loadView('partials/top_nav', null, array());
	}

	/**
	 * Send Output
	 */
	function __destruct(){
		$this->core->template->display();
	}
}
?>