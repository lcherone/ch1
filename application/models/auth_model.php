<?php
/**
 *  @name      CH1 - A Basic Empty MVC PHP Framework
 *  @author    Lawrence Cherone - http://cherone.co.uk
 *  @version   rev0.1a
 */
if (!defined("RUN")){exit(header ("Location: ./"));}

Class auth_model{

	function __construct($core){
		$this->core = $core;

		$sql_file    = 'sqlite:./application/databases/users.db';

		$sql_table[] = "CREATE TABLE `users` (`id` INTEGER PRIMARY KEY,
										      `realname`   TEXT,
										      `username`   TEXT,
										      `pass_hash`  TEXT,
										      `pass_salt`  TEXT,
										      `user_level` INTEGER
											  `active` 	   INTEGER)";

		$this->db = $this->core->model->load('lib/sqlite', $sql_file, $sql_table);

		$this->auth = $this->core->model->load('lib/auth', $this->db);
	}

	function process_login($create_user = null){
		return $this->auth->process_login($create_user);
	}

	function create_admin($create_user = null){
		return $this->auth->process_login($create_user);
	}

	function check_login(){
		if(!empty($_SESSION['logged_in']) && $_SESSION['logged_in'] == true){
			return true;
		}else{
			return false;
		}
	}

}
?>