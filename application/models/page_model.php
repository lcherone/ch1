<?php
/**
 *  @name      CH1 - A Basic Empty MVC PHP Framework
 *  @author    Lawrence Cherone - http://cherone.co.uk
 *  @version   rev0.1a
 */
if (!defined("RUN")){exit(header ("Location: ./"));}

Class page_model{

	function __construct($core){
		$this->core = $core;

		$sql_file = 'sqlite:./application/databases/page.db';

		/* pages table */
		$sql_table[] = 'CREATE TABLE pages (`id` INTEGER PRIMARY KEY,
											`title` 	 	TEXT,
											`slug`  	 	TEXT,
											`menulink` 	 	TEXT,
											`keywords` 	 	TEXT,
											`description`   TEXT,
											`content_main`  TEXT,
											`content_right` TEXT,
											`view` 	 	 	TEXT,
											`pageviews` 	INTEGER,
											`position` 	 	INTEGER,
											`parent` 	 	INTEGER,
											`created` 	 	INTEGER,
											`active` 	 	INTEGER)';
		/* Add Home page */
		$sql_table[] = 'INSERT INTO pages (`title`			,
										   `content_main`	,
										   `view`			,
										   `pageviews`		,
										   `position`		,
										   `parent`			,
										   `created`		,
										   `active`)
							   VALUES     ("Home"			,
										   "Welcome to the homepage!!!",
										   "page"			,
										   "0"				,
										   "0"				,
										   "1"				,
										   "'.time().'"		,
										   "1")';

		$this->db = $this->core->model->load('lib/sqlite', $sql_file, $sql_table);
	}

	function get_page($slug = null){
		$sql = "SELECT *
				FROM pages
				WHERE slug = :slug";
		$sql = $this->db->prepare($sql);
		$sql->bindParam(':slug', $slug, PDO::PARAM_STR);
		$sql->execute();
		return $sql->fetch(PDO::FETCH_ASSOC);
	}

	function update_page(){}

	function delete_page(){}

}
?>