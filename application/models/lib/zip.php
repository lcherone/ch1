<?php
/**
 *  @name      CH1 - A Basic Empty MVC PHP Framework
 *  @author    Lawrence Cherone - http://cherone.co.uk
 *  @version   rev0.1a
 */
if (!defined("RUN")){exit(header ("Location: ../"));}

/**
 * A simple Zip class
 */
class zip {
	/**
     * Add files and sub-directories in a folder to zip file.
     * @param string $folder
     * @param ZipArchive $zipFile
     * @param int $exclusiveLength Number of text to be exclusived from the file path.
     */
	private static function folderToZip($folder, &$zipFile, $exclusiveLength) {
		$handle = opendir($folder);
		while (false !== $f = readdir($handle)) {
			if ($f != '.' && $f != '..') {
				$filePath = $folder."/".$f;
				// Remove prefix from file path before add to zip.
				$localPath = substr($filePath, $exclusiveLength);
				if (is_file($filePath)) {
					$zipFile->addFile($filePath, $localPath);
				} elseif (is_dir($filePath)) {
					// Add sub-directory.
					$zipFile->addEmptyDir($localPath);
					self::folderToZip($filePath, $zipFile, $exclusiveLength);
				}
			}
		}
		closedir($handle);
	}
	/**
     * Zip a folder (include itself).
     *   Usage:
     *   Zippit::zipDir('/path/to/sourceDir', '/path/to/out.zip');
     *
     * @param string $sourcePath Path of directory to be zip.
     * @param string $outZipPath Path of output zip file.
     */
	public function create($sourcePath, $outZipPath)
	{
		$pathInfo   = pathInfo($sourcePath);
		$parentPath = $pathInfo['dirname'];
		$dirName    = $pathInfo['basename'];

		$z = new ZipArchive();
		if (!$z->open($outZipPath, ZIPARCHIVE::CREATE)) {
			return false;
		}
		$z->addEmptyDir($dirName);
		self::folderToZip($sourcePath, $z, strlen($parentPath."/"));
		$z->close();
	}

	public function extract($source, $destination){
		$zip = new ZipArchive;
		if($zip->open($source) === TRUE) {
			$zip->extractTo($destination);
			$zip->close();
			return true;
		} else {
			return false;
		}
	}

}