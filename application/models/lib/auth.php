<?php
/**
 *  @name      CH1 - A Basic Empty MVC PHP Framework
 *  @author    Lawrence Cherone - http://cherone.co.uk
 *  @version   rev0.1a
 */
if (!defined("RUN")){exit(header ("Location: ../"));}

class auth{
	public $db;
	public $user;
	public $pass;
	public $error;
	// sha512
	public $algo = '$6';
	// Cost parameter, 25k iterations
	public $cost = '$rounds=25000$';

	function __construct($db){
		$this->db = $db;
	}

	function make_seed(){
		list($usec, $sec) = explode('.', microtime(true));
		return (float) $sec + ((float) $usec * 100000);
	}

	function unique_salt(){
		$salt = null;
		mt_srand($this->make_seed());
		for($i=0;$i < mt_rand(1,10);$i++){
			$salt = sha1($salt.mt_rand().uniqid().microtime(true));
		}
		return substr($salt,0,16);
	}

	function hash($password){
		$this->salt 	       = $this->unique_salt();
		$this->full_hash 	   = crypt($password, $this->algo.$this->cost.$this->salt);
		$this->full_salt  	   = substr($this->full_hash, 0, 33);
		$this->hashed_password = substr($this->full_hash, 33);
		return $this->full_hash;
	}

	/**
     * Validate the given crypto hash against the given password
     */
	function check_password($hash, $salt, $password){
		$hash = ($this->algo.$this->cost.$salt.'$'.$hash);
		if($hash == crypt($password, substr($hash, 0, 33))){
			//Regenerate a new hash and salt for given password
			$this->update_keys();
			$this->status = true;
			$_SESSION['logged_in'] = true;
			return true;
		}else{
			$this->status = false;
			$this->set_error('global','Username or password do not match!');
			return false;
		}
	}

	function process_login($create = null){
		if($_SERVER['REQUEST_METHOD']=='POST'){

			$this->user   = (isset($_SESSION['userParam']) && isset($_POST[$_SESSION['userParam']])) ? $_POST[$_SESSION['userParam']] : null;
			$this->pass   = (isset($_SESSION['passParam']) && isset($_POST[$_SESSION['passParam']])) ? $_POST[$_SESSION['passParam']] : null;
			$this->create = $create;

			$cont = true;
			if($this->user == null || strlen($this->user) <= 2){
				$this->set_error('user','Please enter a username!');
				$cont = false;
			}
			if($this->pass == null || strlen($this->pass) <= 2){
				$this->set_error('pass','Please enter a password!');
				 $cont=false;
			}

			if($cont == true){
				//Alls good continue, by changing $this->create != null we create an account
				if($this->create != null){
					//Check user for new account
					if($this->check_user() == true){
						$this->set_error('user','Username already taken.');
						return false;
					}
					//Create account
					return $this->create_account(null, null, $create);
				}else{
					return $this->check_login();
				}
			}else{
				//Error with form
				$this->set_error('global','Please fill in login form!');
			}
			return false;
		}
	}

	function check_login(){
		$sql = 'SELECT pass_hash, pass_salt FROM users WHERE username=:username';
		$statement = $this->db->prepare($sql);
		$statement->bindParam(':username', $this->user, PDO::PARAM_STR);
		$statement->execute();
		$result = $statement->fetch(PDO::FETCH_ASSOC);

		return $this->check_password($result['pass_hash'], $result['pass_salt'], $this->pass);
	}

	function update_keys(){
		//Update account password hash & salt
		$this->hash($this->pass);
		$sql = 'UPDATE users SET pass_hash=:pass_hash, pass_salt=:pass_salt WHERE username=:username';
		$statement = $this->db->prepare($sql);
		$statement->bindParam(':username', $this->user, PDO::PARAM_STR);
		$statement->bindParam(':pass_hash', $this->hashed_password, PDO::PARAM_STR);
		$statement->bindParam(':pass_salt', $this->salt, PDO::PARAM_STR);
		$statement->execute();

		$this->status = true;
		$_SESSION['logged_in'] = true;
	}

	function logout(){
		unset($_SESSION['logged_in']);
		session_regenerate_id(true);
		exit(header('Location: '.SITE_URL));
	}

	function set_error($type,$value){
		$this->error[$type]=$value;
	}

	function check_user(){
		$sql = 'SELECT 1 FROM users WHERE username=:username';
		$statement = $this->db->prepare($sql);
		$statement->bindParam(':username', $this->user, PDO::PARAM_STR);
		$statement->execute();
		$result = $statement->fetch(PDO::FETCH_ASSOC);

		if(!empty($result)){return true;}else{return false;}
	}

	function create_account($user=null, $pass=null, $level=null){
		if($user != null){$this->user = $user;}
		if($pass != null){$this->pass = $pass;}

		$this->hash($this->pass);
		if($level != null){
			$sql = 'INSERT INTO users (username,   pass_hash,  pass_salt,  user_level)
						   	   VALUES (:username, :pass_hash, :pass_salt, :user_level)';
			$statement = $this->db->prepare($sql);
			$statement->bindParam(':username',  $this->user, PDO::PARAM_STR);
			$statement->bindParam(':pass_hash', $this->hashed_password, PDO::PARAM_STR);
			$statement->bindParam(':pass_salt', $this->salt, PDO::PARAM_STR);
			$statement->bindValue(':user_level', $level, PDO::PARAM_STR);
		}else{
			$sql = 'INSERT INTO users (username,   pass_hash,  pass_salt)
						   	   VALUES (:username, :pass_hash, :pass_salt)';
			$statement = $this->db->prepare($sql);
			$statement->bindParam(':username',  $this->user, PDO::PARAM_STR);
			$statement->bindParam(':pass_hash', $this->hashed_password, PDO::PARAM_STR);
			$statement->bindParam(':pass_salt', $this->salt, PDO::PARAM_STR);
		}
		$statement->execute();

		$this->status = true;
		$_SESSION['logged_in'] = true;
		return true;
	}

	function get_user_info($user){
		$sql = "SELECT * FROM users WHERE username = :username";
		$sql = $this->db->prepare($sql);
		$sql->bindParam(':username', $user, PDO::PARAM_STR);
		$sql->execute();
		return $sql->fetch(PDO::FETCH_ASSOC);
	}

}
?>