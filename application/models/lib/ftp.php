<?php
/**
 *  @name      CH1 - A Basic Empty MVC PHP Framework
 *  @author    Lawrence Cherone - http://cherone.co.uk
 *  @version   rev0.1a
 */
if (!defined("RUN")){exit(header ("Location: ../"));}

/**
 * A simple FTP crud class
 */
Class ftp{

	public $status;

	function __construct($host, $user, $pass){
		$this->host = $host;
		$this->user = $user;
		$this->pass = $pass;
		$this->status = 'Ready';
	}

	/*Connect*/
	private function connect(){
		if (!isset($this->ftp)){
			$this->ftp = ftp_connect($this->host, 21, 3) or die ("Cannot connect to host");
			ftp_login($this->ftp, $this->user, $this->pass) or die("Cannot login");
			ftp_pasv($this->ftp, true);
			$this->status = 'Connected';
		}
	}

	public function ftp_get_contents($ftp_path,$local_file){
		$this->connect();
		if(ftp_get($this->ftp, $local_file, $ftp_path,  FTP_BINARY)) {
			$this->status = 'Download complete';
		}else{
			$this->status = 'Cannot download';
		}
	}

	public function ftp_put_contents($local_file,$ftp_path){
		$this->connect();
		if(ftp_put($this->ftp, $ftp_path, $local_file, FTP_BINARY)) {
			$this->status = 'Upload complete';
		}else{
			$this->status = 'Cannot upload';
		}
	}

	public function ftp_delete_file($ftp_path){
		$this->connect();
		if (ftp_delete($this->ftp, $ftp_path)) {
			$this->status = "$ftp_path deleted successful";
		}else{
			$this->status = "Could not delete $ftp_path";
		}
	}

	public function ftp_make_dir($dir){
		$this->connect();
		if (ftp_mkdir($this->ftp, $dir)) {
			$this->status = "Successfully created $dir";
		} else {
			$this->status = "Could not create $dir";
		}
	}

	public function ftp_delete_dir($dir){
		$this->connect();
		if (ftp_rmdir($this->ftp, $dir)) {
			$this->status = "Successfully deleted $dir\n";
		} else {
			$this->status = "Could not delete $dir\n";
		}
	}

	public function show_files($dir='/'){
		$this->connect();
		return ftp_nlist($this->ftp, $dir);
	}

	private function close(){
		ftp_close($this->ftp);
	}

	function __destruct(){
		if(isset($this->ftp)){
			$this->close();
		}
	}
}
?>