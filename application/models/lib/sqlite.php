<?php
/**
 *  @name      CH1 - A Basic Empty MVC PHP Framework
 *  @author    Lawrence Cherone - http://cherone.co.uk
 *  @version   rev0.1a
 */
if (!defined("RUN")){exit(header ("Location: ./"));}

class sqlite{
	private $db;

	function __construct($dsn, $setup_query){
		$this->dsn   = $dsn;
		$this->setup = $setup_query;
		$this->check_setup();
	}

	public function connect(){
		try{
			if(!$this->db instanceof PDO){
				$this->db = new PDO($this->dsn);
				$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$this->db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
				$this->db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE,PDO::FETCH_ASSOC);
			}
		}catch(Exception $e){
			die('<pre>'.$e->getMessage().'</pre>');
		}
		return $this;
	}

	public function select($table, $where=null, $id=null, $order=null){
		$this->connect();
		$sql = "SELECT * FROM $table WHERE $where=:id ".(($order!=null)?'ORDER BY id '.$order:null);
		$query = $this->db->prepare($sql);
		$query->bindParam(':id', $id);
		$query->execute();
		return $query->fetchAll(PDO::FETCH_ASSOC);
	}

	public function raw_select($sql){
		$this->connect();
		return $this->db->query($sql)->fetchAll(PDO::FETCH_ASSOC);
	}

	public function query($sql){
		$this->connect();
		$this->db->query($sql);
	}

	public function insert($table, $values){
		$this->connect();
		$fieldnames = array_keys($values[0]);
		$fields = '('.implode(' ,',$fieldnames).')';
		$bounds = '(:'.implode(', :',$fieldnames).')';
		$sql = "INSERT INTO {$table} {$fields} VALUES {$bounds}";
		$query = $this->db->prepare($sql);
		foreach($values as $vals){
			$query->execute($vals);
		}
	}

	public function update($table, $fieldname, $value, $pk, $id){
		$this->connect();
		$sql = "UPDATE `$table` SET `$fieldname`=:value WHERE `$pk` = :id";
		$query = $this->db->prepare($sql);
		$query->bindParam(':id', $id);
		$query->bindParam(':value', $value);
		$query->execute();
	}

	public function delete($table, $fieldname, $id){
		$this->connect();
		$sql = "DELETE FROM `$table` WHERE `$fieldname` = :id";
		$query = $this->db->prepare($sql);
		$query->bindParam(':id', $id, PDO::PARAM_STR);
		$query->execute();
	}

	function check_setup(){
		$dso = explode(':',$this->dsn);
		if(file_exists($dso[1])){
			return;
		}else{
			$this->connect();
			//Setup Table
			if(is_array($this->setup)){
				foreach($this->setup as $query){
					$this->db->query($query);
				}
			}else{
				$this->db->query($this->setup);
			}
			exit(header("Location: ".$_SERVER['REQUEST_URI']));
		}
	}

	/* So we can prepare custom querys using the same connect but from outside this class */
	public function prepare($sql){
		$this->connect();
		return $this->db->prepare($sql);
	}

	public function last_insert_id(){
		$this->connect();
		return $this->db->lastInsertId();
	}
}
?>