<?php
/**
 * Thread processer file
 */
if(php_sapi_name() !== 'cli') exit('cli access only!');

ignore_user_abort(true);
set_time_limit(0);

//Get input arguments
$args = unserialize(base64_decode($_SERVER['argv'][1]));

//Check log and kill file exists
if(!file_exists($args['log_file'])){
	file_put_contents($args['log_file'], null);
}
if(!file_exists($args['kill_file'])){
	file_put_contents($args['kill_file'], null);
}

//Main Processing loop
do{
	//Do lots of importaint stuff hehe
	file_put_contents($args['log_file'], print_r($args,true));

	//Kill switch
	if (file_get_contents($args['kill_file']) != '') {
		die();
	}else{
		sleep($args['sleep_duration']); //do loop again after sleep
	}
}while(true);
?>