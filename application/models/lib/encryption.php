<?php
/**
 *  @name      CH1 - A Basic Empty MVC PHP Framework
 *  @author    Lawrence Cherone - http://cherone.co.uk
 *  @version   rev0.1a
 *
 * Example usage of this class, putting all in $crypt array
 * $crypt 		   = array();
 * $crypt['class'] = new Encryption();
 * $crypt['data']  = 'This data will be encrypted';
 *
 * //Not stored, expected from a value supplied by the user eg a password.
 * $crypt['user_block_key']  = 'The user block key, This is the key for this specific piece of data';
 *
 * //Stored, this key is stored along side the encrypted value in the db
 * $crypt['user_static_key'] = 'User Specific Key, This is Assigned to the user one time and stays with them';
 *
 * //This key is stored on the server out of public view.
 * $crypt['application_key'] = 'This Key is a Static Value thats stored on the system';
 *
 * //Final key, not stored anywhere
 * $crypt['key'] = $crypt['class']->makeKey($crypt['user_block_key'], $crypt['user_static_key'], $crypt['application_key']);
 *
 * //Crypted
 * $crypt['encryptedData'] = $crypt['class']->encrypt($crypt['data'], $crypt['key']);
 * //deCrypted
 * $crypt['decryptedData'] = $crypt['class']->decrypt($crypt['encryptedData'], $crypt['key']);
 */

Class encryption {
	protected $cipher = MCRYPT_RIJNDAEL_256;
	protected $mode = MCRYPT_MODE_CBC;
	protected $rounds =  2048;
	protected $iv_size = 1024;

	/**
     * Decrypt
     */
	public function decrypt($data, $key) {
		$salt = substr($data, 0, $this->iv_size);
		$enc  = substr($data, $this->iv_size, -64);
		$mac  = substr($data, -64);

		list ($cipherKey, $macKey, $iv) = $this->getKeys($salt, $key);

		if ($mac !== hash_hmac('sha512', $enc, $macKey, true)) {
			return false;
		}

		$dec  = mcrypt_decrypt($this->cipher, $cipherKey, $enc, $this->mode, $iv);
		return $this->unpad($dec);
	}

	/**
     * Encrypt
     */
	public function encrypt($data, $key) {
		$salt = mcrypt_create_iv($this->iv_size, MCRYPT_DEV_URANDOM);

		list ($cipherKey, $macKey, $iv) = $this->getKeys($salt, $key);
		$enc = mcrypt_encrypt($this->cipher, $cipherKey, $this->pad($data), $this->mode, $iv);

		return $salt.$enc.hash_hmac('sha512', $enc, $macKey, true);
	}

	/**
     * Generates a set of keys given a random salt and a master key
     *
     * @param string $salt A random string to change the keys each encryption
     * @param string $key  The supplied key to encrypt with
     *
     * @returns array An array of keys (a cipher key, a mac key, and a IV)
     */
	protected function getKeys($salt, $key) {
		$ivSize = mcrypt_get_iv_size($this->cipher, $this->mode);
		$keySize = mcrypt_get_key_size($this->cipher, $this->mode);
		$length = 2 * $keySize + $ivSize;

		$key = $this->pbkdf2('sha512', $key, $salt, $this->rounds, $length);

		$cipherKey = substr($key, 0, $keySize);
		$macKey = substr($key, $keySize, $keySize);
		$iv = substr($key, 2 * $keySize);
		return array($cipherKey, $macKey, $iv);
	}

	/**
	 * Make key secure key from a sub set of keys
	 *
	 * @param unknown_type $userKey
	 * @param unknown_type $serverKey
	 * @param unknown_type $userSuppliedKey
	 * @return unknown
	 */
	function makeKey($userKey, $serverKey, $userSuppliedKey) {
		$key = hash_hmac('sha512', $userKey, $serverKey);
		$key = hash_hmac('sha512', $key, $userSuppliedKey);
		return $key;
	}

	/**
     * Stretch the key using the PBKDF2 algorithm
     *
     * @see http://en.wikipedia.org/wiki/PBKDF2
     *
     * @param string $algo   The algorithm to use
     * @param string $key    The key to stretch
     * @param string $salt   A random salt
     * @param int    $rounds The number of rounds to derive
     * @param int    $length The length of the output key
     *
     * @returns string The derived key.
     */
	protected function pbkdf2($algo, $key, $salt, $rounds, $length) {
		$size   = strlen(hash($algo, '', true));
		$len    = ceil($length / $size);
		$result = '';
		for ($i = 1; $i <= $len; $i++) {
			$tmp = hash_hmac($algo, $salt . pack('N', $i), $key, true);
			$res = $tmp;
			for ($j = 1; $j < $rounds; $j++) {
				$tmp  = hash_hmac($algo, $tmp, $key, true);
				$res ^= $tmp;
			}
			$result .= $res;
		}
		return substr($result, 0, $length);
	}

	protected function pad($data) {
		$length = mcrypt_get_block_size($this->cipher, $this->mode);
		$padAmount = $length - strlen($data) % $length;
		if ($padAmount == 0) {
			$padAmount = $length;
		}
		return $data . str_repeat(chr($padAmount), $padAmount);
	}

	protected function unpad($data) {
		$length = mcrypt_get_block_size($this->cipher, $this->mode);
		$last = ord($data[strlen($data) - 1]);
		if ($last > $length) return false;
		if (substr($data, -1 * $last) !== str_repeat(chr($last), $last)) {
			return false;
		}
		return substr($data, 0, -1 * $last);
	}
}
?>