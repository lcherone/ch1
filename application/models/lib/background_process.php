<?php
/**
 * @example:

    $arguments = array(
	'process_file'=>getcwd().'\process.php',
	'log_file'=>getcwd().'\log.txt',
	'kill_file'=>'kill.loop.txt',
	'sleep_duration'=>1,
	'test2'=>'This is a test var !"£$%^&*()_+{}~@:?><',
	);


	if(BackgroundProcess::fork($arguments['process_file'], null, base64_encode(serialize($arguments)))){
		echo 'Running in background.';
	}else{
		echo 'Failed to run.';
	}
 */
class background_process{

	function fork($script_path, $arguments, $php_exe_path = null){
		$cwd = dirname($script_path);
		if (!is_string($php_exe_path) || !file_exists($php_exe_path)){
			if(strtoupper(substr(PHP_OS, 0, 3)) == 'WIN'){
				$php_exe_path = str_replace('/', '\\', dirname(ini_get('extension_dir'))).'\php.exe';
				if(file_exists($php_exe_path)){
					return $this->open(escapeshellarg($php_exe_path).' '.escapeshellarg($script_path).' '.escapeshellarg($arguments), $cwd);
				}
			}else{
				$php_exe_path = exec("which php-cli");
				if($php_exe_path[0] != '/'){
					$php_exe_path = exec("which php");
				}
				if($php_exe_path[0] == '/'){
					return $this->open(escapeshellarg($php_exe_path).' '.escapeshellarg($script_path).' '.escapeshellarg($arguments), $cwd);
				}
			}
		}else{
			if(strtoupper(substr(PHP_OS, 0, 3)) == 'WIN'){
				$php_exe_path = str_replace('/', '\\', $php_exe_path);
			}
			return $this->open(escapeshellarg($php_exe_path).' '.escapeshellarg($script_path).' '.escapeshellarg($arguments), $cwd);
		}
	}

	function open($exec, $cwd = null){
		if (!is_string($cwd)){
			$cwd = getcwd();
		}
		@chdir($cwd);
		if(strtoupper(substr(PHP_OS, 0, 3)) == 'WIN'){
			if(in_array('com',get_declared_classes())){
				$shell = new COM("WScript.Shell");
				$shell->CurrentDirectory = str_replace('/', '\\', $cwd);
				$shell->Run($exec, 0, false);
				return true;
			}else{
				return false;
			}
		}else{
			if(function_exists('exec')) {
				exec($exec." > /dev/null 2>&1 &");
				return true;
			}else{
				return false;
			}
		}
	}
}
?>