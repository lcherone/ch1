<?php
/**
 *  @name      CH1 - A Basic Empty MVC PHP Framework
 *  @author    Lawrence Cherone - http://cherone.co.uk
 *  @version   rev0.1a
 */
if (!defined("RUN")){exit(header ("Location: ../"));}

/**
 * A simple cURL class
 */
Class curl{
	const USER_AGENT = 'curl';

	/**
	 * CURL GET function
	 */
	function get($url, $referer=null){
		if($referer == null)
			$referer = $url;

		$header = array();
		$header[] = "Accept: text/xml,application/xml,application/json,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5";
		$header[] = "Cache-Control: max-age=0";
		$header[] = "Connection: keep-alive";
		$header[] = "Keep-Alive: 300";
		$header[] = "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7";
		$header[] = "Accept-Language: en-us,en;q=0.5";

		//Check curl is installed or revert to file_get_contents()
		if(function_exists('curl_init') == false){
			$opts = array('http'=>array(
			'method'=>"GET",
			'header'=>implode('\r\n',$header)."\r\n".
			"Referer: $referer\r\n",
			'user_agent'=>self::USER_AGENT));

			$context = stream_context_create($opts);
			return file_get_contents($url, false, $context);
		}

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_USERAGENT, self::USER_AGENT);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
		curl_setopt($curl, CURLOPT_HEADER, 0);
		curl_setopt($curl, CURLOPT_REFERER, $referer);
		curl_setopt($curl, CURLOPT_ENCODING, 'gzip,deflate');
		curl_setopt($curl, CURLOPT_AUTOREFERER, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_TIMEOUT, 30);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

		$html = curl_exec($curl);
		curl_close($curl);
		return $html;
	}

	/**
	 * CURL GET|POST Multi
	 */
	function multi($data = array(array()), $options = array()) {
		$curl = array();
		$result = array();

		$mh = curl_multi_init();
		foreach ($data as $id=>$d) {
			$curl[$id] = curl_init();
			$url = (is_array($d) && !empty($d['url'])) ? $d['url'] : $d;

			$header[0]="Accept: text/xml,application/xml,application/xhtml+xml,application/json";
			$header[0].="text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5";
			$header[]="Cache-Control: max-age=0";
			$header[]="Connection: keep-alive";
			$header[]="Keep-Alive: 2";
			$header[]="Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7";
			$header[]="Accept-Language: en-us,en;q=0.5";
			$header[]="Pragma: ";
			curl_setopt($curl[$id], CURLOPT_URL,            $url);
			curl_setopt($curl[$id], CURLOPT_HEADER,         0);
			curl_setopt($curl[$id], CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl[$id], CURLOPT_TIMEOUT,        30);
			curl_setopt($curl[$id], CURLOPT_USERAGENT,      self::USER_AGENT);
			curl_setopt($curl[$id], CURLOPT_HTTPHEADER,     $header);
			curl_setopt($curl[$id], CURLOPT_REFERER,        $url);
			curl_setopt($curl[$id], CURLOPT_ENCODING,       'gzip,deflate');
			curl_setopt($curl[$id], CURLOPT_AUTOREFERER,    true);
			curl_setopt($curl[$id], CURLOPT_RETURNTRANSFER, true);
			// post?
			if (is_array($d)) {
				if (!empty($d['post'])) {
					curl_setopt($curl[$id], CURLOPT_POST,       1);
					curl_setopt($curl[$id], CURLOPT_POSTFIELDS, $d['post']);
				}
			}
			// extra options?
			if (!empty($options)) {
				curl_setopt_array($curl[$id], $options);
			}
			curl_multi_add_handle($mh, $curl[$id]);
		}
		$running = null;
		do {
			curl_multi_exec($mh, $running);
		} while($running > 0);
		foreach($curl as $id => $c) {
			$result[$id] = curl_multi_getcontent($c);
			curl_multi_remove_handle($mh, $c);
		}
		curl_multi_close($mh);
		return $result;
	}

}