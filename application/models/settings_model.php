<?php
/**
 *  @name      CH1 - A Basic Empty MVC PHP Framework
 *  @author    Lawrence Cherone - http://cherone.co.uk
 *  @version   rev0.1a
 */
if (!defined("RUN")){exit(header ("Location: ./"));}

Class settings_model{

	function __construct($core){
		$this->core = $core;

		$sql_file    = 'sqlite:./application/databases/settings.db';

		$sql_table[] = "CREATE TABLE settings (`id` INTEGER PRIMARY KEY,
											   `key` TEXT,
											   `value` TEXT)";
		/* Add Defaults */
		$sql_table[] = "INSERT INTO settings (`key`,`value`) VALUES ('installed', '0')";
		$sql_table[] = "INSERT INTO settings (`key`,`value`) VALUES ('sitename', 'CH1')";

		$this->db = $this->core->model->load('lib/sqlite', $sql_file, $sql_table);

		$this->settings_get();
	}

	function settings_get(){
		foreach($this->db->raw_select("SELECT * FROM `settings`") as $row){
			$this->{$row['key']} = $row['value'];
		}
		return;
	}

	function setup_database(){
		/* expected POST keys */
		$settings = array(
		"sitename",
		);
		foreach($settings as $setting){
			if(isset($_POST[$setting])){
				$sql = "UPDATE `settings` SET `value` = :value WHERE `key` = :key";
				$qry = $this->db->prepare($sql);
				$qry->bindParam(':key', $setting);
				$qry->bindParam(':value', $_POST[$setting]);
				$qry->execute();
			}
		}
		/* change installed */
		$sql = "UPDATE `settings` SET `value` = :value WHERE `key` = 'installed'";
		$qry = $this->db->prepare($sql);
		$qry->bindValue(':value', '1');
		$qry->execute();
	}

	function settings_update(){
		//Allowed setting keys
		$settings = array(
		"sitename",
		);
		foreach($settings as $setting){
			if(isset($_POST[$setting])){
				$sql = "UPDATE `settings` SET `value` = :value WHERE `key` = :key";
				$qry = $this->db->prepare($sql);
				$qry->bindParam(':key', $setting);
				$qry->bindParam(':value', $_POST[$setting]);
				$qry->execute();
			}
		}
		return true;
	}
}
?>